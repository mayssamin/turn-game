#include "game.h"
#include "game_engine.h"
#include <stdio.h>      
#include <stdlib.h>     
#include <time.h>  
namespace TurnEngine
{

Game::Game()
{
    
    guide = new Block();
   
    guide->SetPosition(glm::vec3(0.0f,0.0f, -2.3f));
    guide->SetScale(glm::vec3(4,4, .001f));
    guide->SetBackgroundTexture("bg.jpg");
    guide->SetForegroundTexture("guide.png");
    GameEngine::getInstance().AddGameObject( (GameObject*) guide ); 
         
}

void Game::StartPlaying(){
    
    if(game_inited)
        return;

    guide->SetEnabled( false );
    
    if(GameEngine::getInstance().network_type == NT_SERVER )
    {
        //SetMyIndex(0);
        srand (time(NULL));
        int turn = rand() % 2 + 1;
        std::cout<< "RANDOM Turn "<<turn<<std::endl;
        GameEngine::getInstance().SendMessage( "T,"+std::to_string(turn)+"," );
        
        SetTurnIndex( turn );
        //std::cout<< "MY Turn "<<turn_index_<<std::endl;
        
    }
    else if( GameEngine::getInstance().network_type == NT_CLIENT )
    {
        //SetMyIndex(1);
       
    
    
  
    mouse = new Block();
    mouse->SetScale(glm::vec3(.2f,.2f, .001f));
    mouse->SetPosition(glm::vec3(0.0f,0.0f, -2.0f));
    mouse->SetBackgroundTexture("cursor1X.jpg");
    if(my_index_ == 2)
        mouse->SetBackgroundTexture("cursor1O.jpg");
    mouse->SetForegroundTexture("cursor2.png");
    GameEngine::getInstance().AddGameObject( (GameObject*) mouse ); 
         
    float initX = -1.0f;
    float initY = 1.0f;
    float curX = initX;

    for(int i = 0; i < 9; i++)
    {
        Block * ball = new Block();
        ball->SetPosition(glm::vec3(curX,  initY, -3.0f));
        ball->SetBackgroundTexture("bg.jpg");
        ball->SetForegroundTexture("empty.png");
        GameEngine::getInstance().AddGameObject( (GameObject*) ball ); 
        ball->c = (i) % 3;
        ball->r = i/3;
        curX += 1;
        if( (i+1)%3 == 0)
        {
            std::cout<<i<<std::endl;
            initY -= 1;
            curX = initX;
        }
        blocks.push_back( ball );
    }

    selectedBlock = blocks.begin();

    glm::vec3 pos = (*selectedBlock)->GetPosition();
    mouse->SetPosition(glm::vec3( pos.x, pos.y, -2) );
 
    wait = new Block();
    wait->SetPosition(glm::vec3(0,0,0));
    wait->SetBackgroundTexture("bg.jpg");
    wait->SetForegroundTexture("wait.png");
    GameEngine::getInstance().AddGameObject( (GameObject*) wait );
    if(my_index_ == turn_index_)
        wait->SetEnabled(false); 
    }
    game_inited = true;
    
}
void Game::Update()
{
    std::queue<std::string>& receivedMessages = GameEngine::getInstance().GetReceivedMessages(); 
    if( receivedMessages.size() > 0 )
    {
        std::string msg = receivedMessages.front();
        const char * text = msg.c_str();
        std::cout<< "Analaysing Message: "<<msg<<" "<<msg.size()<<std::endl;

        receivedMessages.pop();
        if(msg[0] == 'I')
        {
            std::vector<std::string> parts = SplitMessage(msg,',');
            if(parts.size() == 2)
            {
                SetMyIndex( std::stoi(parts[1] ) );
                std::cout<< "Received Index "<< parts[1]<<std::endl;
                
            }
        }
        else if(msg[0] == 'W')
        {
            std::vector<std::string> parts = SplitMessage(msg,',');
            if(parts.size() == 2)
            {
                std::cout << "Winner is "<<(std::stoi(parts[1]) == my_index_ ? "Me!" : "The Other One!") <<std::endl;

                bool win = std::stoi(parts[1]) == my_index_;
            
                result = new Block();
                result->SetPosition( glm::vec3(0,0,0) );
                result->SetBackgroundTexture("bg.jpg");
                result->SetEnabled(true);
                if( win )
                    result->SetForegroundTexture("win.png");
                else
                    result->SetForegroundTexture("lose.png");
                GameEngine::getInstance().AddGameObject( (GameObject*) result ); 
                        
                game_over = true;

            }
        }
        else if(msg[0] == 'P')
        {
            std::vector<std::string> parts = SplitMessage(msg,',');
            if(parts.size() == 4)
            {
                int turn_indx = std::stoi(parts[1]);
                int row = std::stoi(parts[2]);
                int col = std::stoi(parts[3]);
                std::cout << "Player "<<turn_indx<<" Played "<<row<<", "<<col<<std::endl;
                block_flags[row][col] = turn_indx;
                std::list<Block*>::iterator i = blocks.begin();
                while (i != blocks.end())
                {
                    Block* obj = *i;
                    
                    
                    if( obj->r == row && obj->c == col)
                    {
                    Block* nobj = new Block();
                    nobj->SetPosition( obj->GetPosition());
                    nobj->c = obj->c;
                    nobj->r = obj->r;
                    nobj->SetBackgroundTexture("bg.jpg");
                    nobj->SetEnabled(true);
                    if( turn_indx == 1 )
                        nobj->SetForegroundTexture("X.png");
                    else
                        nobj->SetForegroundTexture("O.png");
                    GameEngine::getInstance().AddGameObject( (GameObject*) nobj ); 
                    blocks.push_back(nobj);
                    obj->SetEnabled(false);
                    break;
                }

                if(!(*i)->GetEnabled())
                {
                    blocks.erase(i++);    
                }
                else
                {
                    i++;
                }

            }
            turn_index_ = turn_indx == 1 ? 2 : 1;
                std::cout<<"Turn set to "<<turn_indx<<std::endl;
                if(turn_index_ == my_index_)
                {
                    wait->SetEnabled(false);
                }
                else
                {
                    wait->SetEnabled(true);
                }
                
            }
        }
        else if(msg[0] == 'X')
        {
            std::vector<std::string> parts = SplitMessage(msg,',');
            if(parts.size() == 3)
            {
                int row = std::stoi(parts[1]);
                int col = std::stoi(parts[2]);
                std::cout << "Player Played his turn "<<row<<", "<<col<<std::endl;
                block_flags[row][col] = turn_index_;
                GameEngine::getInstance().SendMessage("P,"+std::to_string(turn_index_)+","+std::to_string(row)+","+std::to_string(col)+"," );
                turn_index_ = (turn_index_ == 1 ? 2 : 1);
            }
        }
        else if(msg[0] == 'T')
        {
            std::vector<std::string> parts = SplitMessage(msg,',');
            if(parts.size() == 2)
            {
            std::cout<< "Received Turn "<<parts[1]<<std::endl;
            SetTurnIndex( std::stoi(parts[1] ) );
            StartPlaying();
            }
        }
    }

    if( !game_over && GameEngine::getInstance().network_type == NT_SERVER )
    {
        int winner_index = WhoWins();

        if(winner_index > 0)
        {
            
            GameEngine::getInstance().SendMessage( "W,"+std::to_string(winner_index)+"," );
            
                    
                    
            game_over = true;
        }
    }
if( GameEngine::getInstance().network_type == NT_CLIENT )
    {if(game_inited && !game_over)
    {
        if(my_index_ == turn_index_)
            wait->SetEnabled(false);
        else
            wait->SetEnabled(true);
    }
    else if(game_over)
    {
            wait->SetEnabled(false);
        
    }
    }
}
void Game::PlayTurn(int row, int col)
{

    std::list<Block*>::iterator i = blocks.begin();
    while (i != blocks.end())
    {
        Block* obj = *i;
        
        
       if( obj->r == row && obj->c == col)
                {
                    Block* nobj = new Block();
                    nobj->SetPosition( obj->GetPosition());
                    nobj->c = obj->c;
                    nobj->r = obj->r;
                    nobj->SetBackgroundTexture("bg.jpg");
                    nobj->SetEnabled(true);
                    if( my_index_ == 1 )
                        nobj->SetForegroundTexture("X.png");
                    else
                        nobj->SetForegroundTexture("O.png");
                    GameEngine::getInstance().AddGameObject( (GameObject*) nobj ); 
                    blocks.push_back(nobj);
                    obj->SetEnabled(false);
                    break;
                }

        if(!(*i)->GetEnabled())
        {
            
            blocks.erase(i++);
            
            }
        else
        {
            i++;
        }

    }

    //if(GetTurnIndex() == GetMyIndex()){
        block_flags[row][col] = my_index_;
        SetTurnIndex( GetTurnIndex() == 0 ? 1 : 0 );
        std::cout<< "Sending Turn Pass " << std::endl;
        GameEngine::getInstance().SendMessage( "X,"+std::to_string(row)+","+std::to_string(col)+"," );
        //GameEngine::getInstance().SendMessage("T");
    //}
}

int Game::GetTurnIndex(){
    //mu.lock();
    return turn_index_;
    //mu.unlock();
}
int Game::GetMyIndex(){
    return my_index_;
}

void Game::SetTurnIndex( int new_turn)
{
    //mu.lock();
    turn_index_ = new_turn;
    //mu.unlock();
}
void Game::SetMyIndex( int new_index){
    //mu2.lock();
    my_index_ = new_index;
    //mu2.unlock();
}

void Game::ProcessEvent(NETWORK_EVENT event, std::string data) 
{
    switch (event)
    {
    case NE_CLIENT_CONNECTED:
        players.push_back(data);
        GameEngine::getInstance().SendMessage( std::stoi(data), "I,"+std::to_string( players.size() )+"," );
        if(GameEngine::getInstance().GetNumberOfPlayers() == 2)
            StartPlaying();
        break;
    case NE_CONNECTED_TO_SERVER:
        //StartPlaying();
        break;
    case NE_SERVER_RUNNING:
        guide->SetEnabled(false);
        break;
    case NE_CONNECTION_LOST:
        
        break;
    case NE_CONNECTION_REFUSED:
        exit( 1 );
        break;
    default:
        break;
    }
}
void Game::MouseClicked() 
{
   
}

void Game::KeyPressed( int key ) 
{
    if( game_inited && !game_over && my_index_ == turn_index_ )
    {
    int col = (*selectedBlock)->c;
    int row = (*selectedBlock)->r;
    switch ( key )
        {
        case  GLFW_KEY_UP:
            
            row--;
            if( row < 0 )
                row+=3;
            break;
            
            
        case GLFW_KEY_DOWN:
         
            row++;
            if( row > 2 )
                row-=3;
         break;
         case GLFW_KEY_RIGHT:
         
             col++;
             if(col > 2)
                col -= 3;
         break;
    case GLFW_KEY_LEFT:
         
            col--;
             if(col < 0 )
                col += 3;

                break;

                case GLFW_KEY_SPACE:
                    PlayTurn(row, col);
                break;
         }

         for(std::list<Block*>::iterator it = blocks.begin(); it != blocks.end(); it++ )
            {
                
                if( (*it)->GetEnabled() && (*it)->r == row && (*it)->c == col )
                {
                    selectedBlock = it;
                    glm::vec3 pos = (*selectedBlock)->GetPosition();
                    mouse->SetPosition(glm::vec3( pos.x, pos.y, -2) );
                    break;
                }
            }
    }
}

int Game::WhoWins()
{
    int winner = -1;
    for(int i =0 ; i <3;i++)
    {
        if(block_flags[i][0] == block_flags[i][1] && block_flags[i][0] == block_flags[i][2] )  
            {
                winner = block_flags[i][0];
                break;
            }
        else if( block_flags[0][i] == block_flags[1][i] && block_flags[0][i] == block_flags[2][i])
        {
                winner = block_flags[0][i];
                break;
        }
    }
    if(block_flags[0][0] == block_flags[1][1] && block_flags[0][0] == block_flags[2][2])
    {
        winner = block_flags[0][0];
    }
    else
    {
        if(block_flags[0][2] == block_flags[1][1] && block_flags[0][2] == block_flags[2][0])
    
        winner = block_flags[0][2];
    
    }
    return winner;
}

}