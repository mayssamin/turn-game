#ifndef GAME_H
#define GAME_H
#include <mutex>
#include <list>
#include "game_behaviour.h"
#include "network_listener.h"

#include "ball.h"

namespace TurnEngine{
class Game : GameBehaviour, NetworkListenerInterface
{          
    private:
        std::mutex mu, mu2;
        
        bool game_inited = false;  
        int turn_index_ = -1;
        int my_index_ = -1;
        bool game_over = false;
        int block_flags[3][3] = { {-1,-1,-1}, {-1,-1,-1}, {-1,-1,-1} };
        std::list<Block*> blocks;
        Block *mouse, *result, *wait, *guide;
        std::list<Block*>::iterator selectedBlock;
        std::list<std::string> players;
    public:
        Game();
        void StartPlaying();
        void Update() override;
        void Finish() override {}
        void MouseClicked() override;
        void KeyPressed( int key ) override;
        void PlayTurn(int xpos, int ypos);

        int GetTurnIndex();
        int GetMyIndex();

        void SetTurnIndex(int new_turn);
        void SetMyIndex(int new_index);

        void ProcessEvent(NETWORK_EVENT event, std::string data) override;

        int WhoWins();
        
    };

}


#endif //GAME_H