#include <stdio.h>

#include <sys/types.h>

#include <sys/socket.h>

#include <netinet/in.h>

#include <thread>
#include <iostream>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#define _POSIX_SOURCE
#include <unistd.h>

#include "game_engine.h"
#include "game.h"

using namespace TurnEngine;

int main(int argc, char** argv)
{

  signal( SIGCHLD,SIG_IGN );
  
  
  GameEngine::getInstance().Init();
  Game * game = new Game();
  GameEngine::getInstance().AddBehaviour( (GameBehaviour*) game );
  GameEngine::getInstance().AddNetworkListener( (NetworkListenerInterface*) game );

  GameEngine::getInstance().Run();
  return 0;
}