
#include "renderer.h"
#include "game_engine.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>


namespace TurnEngine{
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods);
Renderer::Renderer(/* args */)
{
}

Renderer::~Renderer()
{
}

void Renderer::Init()
{

    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

#ifdef __APPLE__
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // uncomment this statement to fix compilation on OS X
#endif

    // glfw window creation
    // --------------------
    window_ = glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT, "XO", /*glfwGetPrimaryMonitor()*/ NULL, NULL);
    if (window_ == NULL)
    {
        std::cout << "Failed to create GLFW window" << std::endl;
        glfwTerminate();
        throw std::runtime_error( "Failed to initialize GL Window!" );
    }
    glfwMakeContextCurrent(window_);
    glfwSetFramebufferSizeCallback(window_, framebuffer_size_callback);
    glfwSetCursorPosCallback(window_, mouse_callback);
    glfwSetMouseButtonCallback(window_, mouse_button_callback);
    glfwSetKeyCallback(window_,key_callback);
// tell GLFW to capture our mouse
    //glfwSetInputMode(window_, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
    // glad: load all OpenGL function pointers
    // ---------------------------------------
    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
    {
        std::cout << "Failed to initialize GLAD" << std::endl;
        throw std::runtime_error( "Failed to initialize GLAD!" );
    }

glEnable(GL_DEPTH_TEST);


   
}

void Renderer::Run(){
        // input
        // -----
    GameEngine::getInstance().ProcessInput(window_);
    float currentFrame = glfwGetTime();
    deltaTime = currentFrame - lastFrame;
    lastFrame = currentFrame;


        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); 
         //if(GameEngine::getInstance().network_type == NT_SERVER )
             
        // if(game_->GetTurnIndex() != -1 && game_->GetMyIndex() == game_->GetTurnIndex() ){
             
        //     glBindVertexArray(VAO); 
        //     glDrawArrays(GL_TRIANGLES, 0, 3);
        // }
        for(auto object: GameEngine::getInstance().GetGameObjects())
        {
//std::cout<<"Drawing "<<object<<std::endl;
            object->Draw();
        }

        glfwSwapBuffers(window_);
        glfwPollEvents();
    

}
void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
    // make sure the viewport matches the new window dimensions; note that width and 
    // height will be significantly larger than specified on retina displays.
    glViewport(0, 0, width, height);
}
void mouse_callback(GLFWwindow* window, double xpos, double ypos)
{
    GameEngine::getInstance().MouseMoved(xpos, ypos);
}
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    GameEngine::getInstance().key_callback(window, key, scancode,action, mods);
}
void mouse_button_callback(GLFWwindow* window, int button, int action, int mods)
{
    if(button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS) 
    {
           double xpos, ypos;
       //getting cursor position
        glfwGetCursorPos(window, &xpos, &ypos);
        GameEngine::getInstance().MouseMoved(xpos, ypos);
        GameEngine::getInstance().MouseClicked();
    
    }




	

 
}

// void processInput(GLFWwindow *window)
// {
//     if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
//         glfwSetWindowShouldClose(window, true);
//     else if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
//         GameEngine::getInstance().StartServer();
//     else if (glfwGetKey(window, GLFW_KEY_C) == GLFW_PRESS)
//         GameEngine::getInstance().StartClient();
// }

bool Renderer::IsWindowOpen(){
    return !glfwWindowShouldClose(window_);
}

void Renderer::Finish()
{
    // glfw: terminate, clearing all previously allocated GLFW resources.
    // ------------------------------------------------------------------
    glfwTerminate();
}

}