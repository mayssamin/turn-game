#include "game_engine.h"

namespace TurnEngine{

void GameEngine::Init(){
    renderer_ = new Renderer();
    renderer_->Init();

    game_network = new GameNetwork();
    
}

void GameEngine::Run(){
    
    while( renderer_->IsWindowOpen())
    {
        double now = (double) glfwGetTime();

        if( now - last_render_ > render_delay )
        {
            renderer_->Run();
            last_render_ = now;
            
            for(auto behaviour: behaviours_)
            {
                behaviour->Update();
            }
        }
    }   
    for(auto behaviour: behaviours_)
    {
        behaviour->Finish();
    }
    
    renderer_->Finish(); 
    game_network->ShutDown(); 
}

void GameEngine::StartServer()
{
    if(network_type == NT_NONE)
    {
        network_type = NT_SERVER;
        std::thread * t1;
        std::cout << "SERVER MODE"<< std::endl;
        game_network->StartServer();
    }
}

void GameEngine::StartClient()
{
    if(network_type == NT_NONE)
    {
        network_type = NT_CLIENT;
        std::thread * t1;
        std::cout << "CLIENT MODE"<< std::endl;
        game_network->StartClient();
    }
}

void GameEngine::AddBehaviour(GameBehaviour* behaviour)
{
    behaviours_.push_back(behaviour);
}

void GameEngine::AddGameObject( GameObject* gameObject )
{
    game_objects_.push_back( gameObject );
}

std::queue<std::string>& GameEngine::GetReceivedMessages()
{
    
    return game_network->GetReceiveQueue();
}

void GameEngine::SendMessage( std::string message )
{
    game_network->PushMessage( message );
}
void GameEngine::SendMessage( int fd, std::string message )
{
    game_network->PushMessage( fd, message );
}

void GameEngine::AddNetworkListener(NetworkListenerInterface* listener)
{
    network_listeners_.push_back( listener );
}

void GameEngine::NetworkEvent(NETWORK_EVENT event, std::string data)
{
    for(auto nt_listener: network_listeners_)
    {
        nt_listener->ProcessEvent(  event , data);
    }
}

void GameEngine::MouseClicked()
{
    for(auto behaviour: behaviours_)
    {
        behaviour->MouseClicked();
    }
}
void GameEngine::MouseMoved(double xpos, double ypos)
{
    mouseX = xpos;
    mouseY = ypos;
}

void GameEngine::ProcessInput(GLFWwindow *window)
{
     if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
         glfwSetWindowShouldClose(window, true);
     else if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
         GameEngine::getInstance().StartServer();
     else if (glfwGetKey(window, GLFW_KEY_C) == GLFW_PRESS)
         GameEngine::getInstance().StartClient();
    else
    {
        
    }
}

void GameEngine::key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    if ( action == GLFW_PRESS)
    {        
        for(auto behaviour: behaviours_)
        {
            behaviour->KeyPressed(key);
        }
    }
}
}