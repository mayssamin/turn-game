#include "game_network.h"
#include "game_engine.h"

namespace TurnEngine{

GameNetwork::GameNetwork(  )
{

}

GameNetwork::~GameNetwork()
{
}

void GameNetwork::StartClient(){
  std::thread* t1 = new std::thread( [this] 
    {
      try
      { 
        this->ClientWorker();
      }
      catch(const NetworkException& e)
      {
        std::cerr << e.what() << '\n';
        GameEngine::getInstance().NetworkEvent( NE_SERVER_START_FAILED , "");
      } 
    } );
}
void GameNetwork::StartServer(){
  std::thread* t1 = new std::thread( [this] 
    {
      try
      {
        this->ServerWorker();
      }
      catch(const NetworkException& e)
      {
        std::cerr << e.what() << '\n';
      }
      catch(const std::exception& e )
      {
        std::cerr << e.what() << '\n';
      }
        
    } );
}

std::queue<std::string>& GameNetwork::GetReceiveQueue()
{
  return receive_queue;
}

void GameNetwork::ReceiveMessage(int socket, std::string buf)
{
  char buffer[256];
  ssize_t bytes_read;
  std::string messages = buf;
  do 
  {
    bzero(buffer,256);
    bytes_read = recv(socket, buffer, sizeof(buffer), MSG_DONTWAIT);

    if (bytes_read > 0) 
    {
        std::cout << ">>>>> Received ... " << buffer << std::endl;
        messages += buffer;    
    }

  } while (bytes_read > 0);
  
  if(messages != ""){
    std::cout << ">>>>> Received ... " << messages <<"Size " <<messages.size()<<std::endl;
    char delim = '\t';
    std::vector<std::string> parts = SplitMessage(messages, delim );
    for (auto x : parts)
    {
      std::cout <<">>>>> "<< x <<", "<< x.size()<<' ';
      GameNetwork::receive_queue.push( x );
      }
    std::cout << std::endl;

  }
}

 void GameNetwork::ServerWorker ()
 {
  int sockfd, newsockfd, portno, n, pid;
  socklen_t clilen;
  char buffer[256];
  struct sockaddr_in serv_addr, cli_addr;
  
  sockfd = socket(AF_INET, SOCK_STREAM, 0);
  if (sockfd < 0)
    throw NetworkException( "received negative value" );

  bzero((char *) &serv_addr, sizeof(serv_addr));

  portno = 4132;
  serv_addr.sin_family = AF_INET;
  serv_addr.sin_port = htons(portno);
  serv_addr.sin_addr.s_addr = INADDR_ANY;
  
  if( bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr) ) < 0)
    throw NetworkException("ERROR on binding");
  
  listen(sockfd,5);
  
  GameEngine::getInstance().NetworkEvent(NE_SERVER_RUNNING, "");
  std::cout << "Waiting for a client ... " << std::endl;

  pollfd server_fd;
  server_fd.fd = sockfd;
  server_fd.events = POLLIN;
  poll_fd_list_.push_back( server_fd );
  
  int pollCount = 1;

  while(true)
  {
    struct timeval to;
    to.tv_sec = 0;
    to.tv_usec = 0;

    int poll_count = poll(&poll_fd_list_[0], 3, 0);
    if( poll_count == -1 ) 
    {
        perror("poll");
        exit(1);
    }

    for(int i = 0; i < pollCount; i++) 
    {
      // Check if someone's ready to read
        if (poll_fd_list_[i].revents & POLLIN) 
        { 
          std::cout << "Someone's knocking the door... "<<std::endl;
          if (poll_fd_list_[i].fd == sockfd) 
          {
            newsockfd = accept(sockfd,(struct sockaddr *) &cli_addr, &clilen);

            if (newsockfd == -1) 
            {
              perror("accept");
            } 
            else 
            {
              pollfd client_fd;
  
              client_fd.fd = newsockfd;
              client_fd.events = POLLIN;
              poll_fd_list_.push_back( client_fd );
              pollCount++;
              GameEngine::getInstance().NetworkEvent(NE_CLIENT_CONNECTED, std::to_string(newsockfd) );
            }
          }
          else 
          {
            char buf[256];
            int nbytes = recv(poll_fd_list_[i].fd, buf, sizeof buf, 0);

            int sender_fd = poll_fd_list_[i].fd;
            if (nbytes <= 0) 
            {
              // Got error or connection closed by client
              if (nbytes == 0) 
              {
                // Connection closed
                std::cout<<"pollserver: socket "<<sender_fd<< " hung up"<<std::endl;
                GameEngine::getInstance().NetworkEvent(NE_CONNECTION_LOST, std::to_string( sender_fd ) );
              } 
              else 
              {
                perror("recv");
              }

              close(poll_fd_list_[i].fd); 
              
              poll_fd_list_[i].fd = -1;
              pollCount--;

            } 
            else 
            {
              ReceiveMessage(poll_fd_list_[i].fd, buf);
            }
        } // END handle data from client
              
      }
    }

    if( !GameNetwork::send_queue.empty() )
    {
      std::pair<int, std::string> msg = GameNetwork::send_queue.front();
      if(msg.first == -1)
      {
        for(auto cli : poll_fd_list_)
        {
          if( cli.fd != server_fd.fd)
          {
            std::cout << "message to send to "<<cli.fd<<": "<<msg.second << std::endl;
          int n = send(cli.fd, msg.second.c_str(), msg.second.size(), 0);
          
          if (n < 0) 
            throw NetworkException("ERROR writing to socket");
          else        
            std::cout << "Sent "<<msg.second << std::endl;
          }
        }
      }
      else
      {
        std::cout << "message to send: "<<msg.second << std::endl;
        int n = send(msg.first, msg.second.c_str(), msg.second.size(), 0);
        
        if (n < 0) 
          throw NetworkException("ERROR writing to socket");
        else        
          std::cout << "Sent "<<msg.second << std::endl;
      }
      GameNetwork::send_queue.pop();
    }
  }
  
}
void GameNetwork::ClientWorker ()
{
  int sockfd, portno, n;
  struct sockaddr_in serv_addr;
  struct hostent *server;
  
  portno = 4132;
  sockfd = socket(AF_INET, SOCK_STREAM, 0);
  
  if (sockfd < 0)
    throw NetworkException("ERROR opening socket");
  
  server = gethostbyname("localhost");

  if (server == NULL)
  {
    fprintf(stderr,"ERROR, no such host");
    exit(0);
  }
  bzero((char *) &serv_addr, sizeof(serv_addr));
  serv_addr.sin_family = AF_INET;
  bcopy((char *)server->h_addr,
      (char *)&serv_addr.sin_addr.s_addr,
      server->h_length);
  serv_addr.sin_port = htons(portno);

  if ( connect( sockfd, (struct sockaddr *) &serv_addr, sizeof( serv_addr ) ) < 0 )
  {
    std::cout << "Connection Refused! "<<std::endl;
    GameEngine::getInstance().NetworkEvent(NE_CONNECTION_REFUSED,"");
  }  

  std::cout << "Connected to server! "<<std::endl;
  GameEngine::getInstance().NetworkEvent(NE_CONNECTED_TO_SERVER,"");

  while (true)
  {
    if(!send_queue.empty())
    {
      std::pair<int, std::string> msg = send_queue.front();
      std::cout << "message to send: "<<msg.second << std::endl;
      int n = send(sockfd, msg.second.c_str(), msg.second.size(), 0);
                  
      std::cout << "Sent "<<msg.second << std::endl;
      send_queue.pop();
  
      if (n < 0) 
        throw NetworkException("ERROR writing to socket");

    }
    ReceiveMessage(sockfd,"");
  }
}

void GameNetwork::PushMessage(std::string msg){
  std::pair<int, std::string> m(-1, msg+"\t");
  send_queue.push( m );
}

void GameNetwork::PushMessage(int fd, std::string msg){
  send_queue.push(std::pair<int, std::string>(fd, msg+"\t") );
}

void GameNetwork::ShutDown(){
  //close(sock);
}

}