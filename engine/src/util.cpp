#include "util.h"
#include <vector>
#include <iostream>

std::vector<std::string> SplitMessage(const std::string &s, char delim) {
  int indx = s.find(delim);
  
  int start = 0;
  std::vector<std::string> result;
  do {
    std::string item = s.substr(start, indx - start  );
    std::cout << "Sub ... " << item <<std::endl;
    start = indx+1;
    result.push_back(item);
    indx = s.find(delim,start);
    // elems.push_back(std::move(item)); // if C++11 (based on comment from @mchiasson)
  }while (indx > -1 );
  return result;
}