#ifndef RENDERER_H_
#define RENDERER_H_

#include "util.h"

#include <iostream>

//#include "game.h"



namespace TurnEngine{

void framebuffer_size_callback(GLFWwindow* window, int width, int height);
void mouse_callback(GLFWwindow* window, double xpos, double ypos);
void mouse_button_callback(GLFWwindow* window, int button, int action, int mods);
void processInput(GLFWwindow *window);

class Renderer
{
private:
    GLFWwindow* window_;
    
public:

    // timing
    float deltaTime = 0.0f;	// time between current frame and last frame
    float lastFrame = 0.0f;

    Renderer(/* args */);
    ~Renderer();
    void Init();
    void Run();
    bool IsWindowOpen();
    void Finish();
};



}
#endif //RENDERER_H_