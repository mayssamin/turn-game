#ifndef GAME_BEHAVIOUR_H_
#define GAME_BEHAVIOUR_H_
/*
    This class is used to define objects that need to be updated in each frame and also want to be informed about mouse and key input.
    To create a game, you need to inherit from this class and implement virtual function. 
*/
#include "util.h"
namespace TurnEngine
{

class GameBehaviour
{
private:
    
public:
    virtual void Update() = 0; //This would be called once a frame
    virtual void Finish() = 0;
    virtual void MouseClicked() = 0; 
    virtual void KeyPressed( int key) = 0;
    GameBehaviour();
    ~GameBehaviour();
};

}

#endif //GAME_BEHAVIOUR_H_