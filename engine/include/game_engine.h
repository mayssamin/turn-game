#ifndef GAME_ENGINE_H_
#define GAME_ENGINE_H_

#include <list>
#include "renderer.h"
#include "game_network.h"
#include "game_object.h"
#include "game_behaviour.h"
#include "network_listener.h"
/*
    TurnEngine is a singleton that manages all other components of game.
*/

namespace TurnEngine
{

class GameEngine
{

    GameNetwork * game_network;
public:
        
        static GameEngine& getInstance()
        {
            static GameEngine    instance_; 
            return instance_;
        }
        NETWORK_TYPE network_type = NT_NONE;
    private:
        GameEngine() {}                    
        Renderer * renderer_;
        std::list<GameBehaviour*> behaviours_;
        std::list<GameObject*> game_objects_;
        std::list<NetworkListenerInterface*> network_listeners_;
        double last_render_ = 0;
        std::chrono::steady_clock::time_point begin;
        const double render_delay = 0.015; 
        double mouseX, mouseY;
        
    public:
        GameEngine(GameEngine const&) = delete;
        void operator=(GameEngine const&)  = delete;

        void Init();
        void Run();
        void StartServer();
        void StartClient();
        void AddBehaviour(GameBehaviour* behaviour);
        void AddGameObject(GameObject* gameObject);
        void AddNetworkListener(NetworkListenerInterface* listener);
        std::queue<std::string>& GetReceivedMessages();
        void SendMessage(std::string message);
        void SendMessage(int fd, std::string message);
        void NetworkEvent(NETWORK_EVENT event, std::string data);
        void MouseClicked();
        void MouseMoved(double xpos, double ypos);
        double GetMouseX(){return mouseX;}
        double GetMouseY(){return mouseY;}
        std::list<GameObject*>& GetGameObjects(){ return game_objects_; }
        void ProcessInput(GLFWwindow *window);
        void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods);
        int GetNumberOfPlayers(){ return game_network->GetNumberOfClients() -1; }
};

}

#endif //GAME_ENGINE_H_