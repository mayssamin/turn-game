#ifndef NETWORK_LISTENER_H_
#define NETWORK_LISTENER_H_
#include "game_network.h"
namespace TurnEngine
{
    class NetworkListenerInterface
    {
    private:
        /* data */
    public:
        NetworkListenerInterface(/* args */){}
        virtual void ProcessEvent( NETWORK_EVENT event , std::string data) = 0;
        ~NetworkListenerInterface(){}
    };
    
    
    
}

#endif //NETWORK_LISTENER_H_