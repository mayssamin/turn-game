#ifndef GAME_NETWORK_H_
#define GAME_NETWORK_H_

#include <stdio.h>
#include <sys/types.h>
#include <thread>
#include <iostream>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#define _POSIX_SOURCE
#include <unistd.h>
#include <queue>
#include <vector>
#include <poll.h>
#include "network_exception.h"
#include <mutex>

namespace TurnEngine
{   
enum NETWORK_TYPE
{
    NT_NONE,
    NT_CLIENT,
    NT_SERVER
};

enum NETWORK_EVENT
{
    NE_NONE,
    NE_CONNECTED_TO_SERVER,
    NE_SERVER_RUNNING,
    NE_CLIENT_CONNECTED,
    NE_CONNECTION_LOST,
    NE_SERVER_START_FAILED,
    NE_CONNECTION_REFUSED
};

class GameNetwork
{
private:
std::mutex mu;
    std::vector<pollfd> poll_fd_list_;
    std::queue< std::string> receive_queue;
    std::queue< std::pair<int, std::string > > send_queue;
    void ServerWorker();
    void ClientWorker();
    void ReceiveMessage(int socket, std::string buf);
    
public:
    
    GameNetwork();
    ~GameNetwork();
    void StartClient();
    void StartServer();
    void PushMessage(std::string msg);
    void PushMessage(int fd, std::string msg);
    void ShutDown();
    std::queue<std::string>& GetReceiveQueue();
    int GetNumberOfClients() { return poll_fd_list_.size();}
};
    
}

#endif
