#ifndef GAME_OBJECT_H_
#define GAME_OBJECT_H_
#include "util.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <shader_m.h>
#include "stb_image.h"

namespace TurnEngine
{

class GameObject
{
protected:
    glm::vec3 scale;
    unsigned int texture1, texture2;
    Shader * ourShader;
    unsigned int VBO, VAO;
    glm::vec3 position;
    std::vector<float>  vertices;
    std::vector<unsigned int> indices;
    
    bool initialised = false;
    std::string texture_background_ = "./container.jpg";
    std::string texture_foreground_ = "./awesomeface.png";
    bool enabled = true;
public:

    GameObject(){
        scale = glm::vec3(1,1,1);
    }
    ~GameObject();
    void Init();
    void Draw();
    virtual void Update() = 0;
    virtual void Finish() = 0; 
    glm::vec3 GetPosition(){return position;}
    void SetPosition(glm::vec3 new_position){ position = new_position; }
    void SetScale( glm::vec3 newScale){ scale = newScale; }
    void SetBackgroundTexture( std::string path){ texture_background_ = path;}
    void SetForegroundTexture( std::string path){ texture_foreground_ = path;}
    void SetEnabled(bool is_enabled){ enabled = is_enabled;}
    bool GetEnabled(){ return enabled;}
};

}

#endif //GAME_OBJECT_H_