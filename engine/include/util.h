#ifndef UTIL_H
#define UTIL_H

#include <glad/glad.h>
 #include <GLFW/glfw3.h>

#include <map>
#include <string>
#include <vector>
#include <string>
#include <iostream>

const unsigned int SCR_WIDTH = 800;
const unsigned int SCR_HEIGHT = 600;

std::vector<std::string> SplitMessage(const std::string& input, const char delim);

#endif //UTIL_H