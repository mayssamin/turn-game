# Turn Engine

Created by Meysam Mohammadi as an assignment for Advance Game Programming class at CGL

## To build the project:

* mkdir build
* cd build
* cmake ..
* make
